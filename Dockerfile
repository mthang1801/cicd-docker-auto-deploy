FROM node:16-alpine
WORKDIR /app 
RUN npm install -g pm2 
COPY package*.json . 
RUN npm install --silent 
COPY . . 
CMD ["pm2-runtime", "ecosystem.config.js"]